<?php

namespace Modules\Core\Models;


use App\Models\BaseModel;
use App\Traits\HasPathTree;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class Department.
 *
 * @package namespace Modules\Core\Models;
 */
class Department extends BaseModel
{
    use HasPathTree, BelongsToTenant;

    public $table = 'core_departments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'parentid', 'path', 'level', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Modules\Core\Models\User', 'core_department_user','department_id', 'id');
    }
}
