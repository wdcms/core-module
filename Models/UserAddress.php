<?php

namespace Modules\Core\Models;

use App\Models\BaseModel;
use App\Traits\HasCreateBy;

/**
 * Class UserAddress.
 *
 * @package namespace App\Models;
 */
class UserAddress extends BaseModel
{
    use HasCreateBy;

    public $table = 'core_user_addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','province', 'city', 'district', 'address', 'zip', 'contact_name', 'contact_phone', 'lastused_at'];

}
