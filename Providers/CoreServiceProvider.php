<?php

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\Livewire;
use Modules\Core\Console\DemoComand;
use Modules\Core\Http\Livewire\Auth\Login;
use Modules\Core\Http\Livewire\Auth\Register;
use Modules\Core\Http\Livewire\Auth\Verify;
use Spatie\Permission\Middlewares\RoleMiddleware;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Core';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'core';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        //$this->commands();

        // 注册组件
        $this->callAfterResolving(BladeCompiler::class, function () {
            $this->registerComponents();
        });

        // 注册中间件
        $this->addMiddlewareAlias('role', RoleMiddleware::class);
        $this->addMiddlewareAlias('permission', PermissionMiddleware::class);
    }


    /**
     * @param $name
     * @param $class
     * @return mixed
     */
    protected function addMiddlewareAlias($name, $class)
    {
        $router = $this->app['router'];

        // 判断aliasMiddleware是否在类中存在
        if (method_exists($router, 'aliasMiddleware')) {
            // aliasMiddleware 顾名思义,就是给中间件设置一个别名
            return $router->aliasMiddleware($name, $class);
        }

        return $router->middleware($name, $class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        require_once __DIR__ . '/../helpers.php';
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    public function registerComponents()
    {
        $components = [
            'modules.core.http.livewire.auth.login' => Login::class,
            'modules.core.http.livewire.auth.register' => Register::class,
            'modules.core.http.livewire.auth.verify' => Verify::class,
        ];

        foreach ($components as $alias => $component) {
            Livewire::component($alias, $component);
        }

        foreach([
            \Modules\Core\View\Components\SubMenu::class => 'submenu',
            \Modules\Core\View\Components\Card::class => 'card',

            // Form
            \Modules\Core\View\Components\Region::class => 'region',
            \Modules\Core\View\Components\Form::class => 'form',
            \Modules\Core\View\Components\FormItem::class => 'formItem',

            // Inputs
            \Modules\Core\View\Components\Inputs\Text::class => 'input',
            \Modules\Core\View\Components\Inputs\TextArea::class => 'input.textarea',
            \Modules\Core\View\Components\Inputs\Date::class => 'input.date',
            \Modules\Core\View\Components\Inputs\DateRange::class => 'input.dateRange',
            \Modules\Core\View\Components\Inputs\Rate::class => 'input.rate',
            \Modules\Core\View\Components\Inputs\Select::class => 'input.select',
            \Modules\Core\View\Components\Inputs\XSelect::class => 'input.xselect',
            \Modules\Core\View\Components\Inputs\Checkbox::class => 'input.checkbox',
            \Modules\Core\View\Components\Inputs\Radio::class => 'input.radio',

            \Modules\Core\View\Components\Inputs\Editor::class => 'input.editor',
            \Modules\Core\View\Components\Inputs\MEditor::class => 'input.meditor',

            \Modules\Core\View\Components\Inputs\Imgs::class => 'input.imgs',

            \Modules\Core\View\Components\Inputs\File::class => 'input.file',
            \Modules\Core\View\Components\Inputs\Media::class => 'input.media',

            // bashboard
            \Modules\Core\View\Components\Dashboards\UserCount::class => 'userCount',
            // \Modules\Core\View\Components\Calendar::class => 'calendar',

        ] as $_class => $_name){
            Blade::component($_class, $_name, 'wd');
        };
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
