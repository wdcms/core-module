const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

var tailwindcss = require('tailwindcss');

mix.setPublicPath('../../public/dist').mergeManifest();

mix.copyDirectory('Resources/assets/imgs', '../../public/dist/core');
mix.js('Resources/assets/js/app.js', 'core/core.js')
mix.sass('Resources/assets/sass/app.scss', 'core/core.css').options({
    processCssUrls: false,
    postCss: [tailwindcss('tailwind.config.js')]
});

if (mix.inProduction()) {
    mix.version();
}
