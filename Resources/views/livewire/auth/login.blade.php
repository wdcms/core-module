@section('title', 'Sign in to your account')

<div class="flex flex-col overflow-y-auto md:flex-row">
    <div class="md:h-auto md:w-1/2">
        @inject('inspiredService', 'Modules\Core\Actions\EveryDayInspired')
        {!! $inspiredService->run() !!}
    </div>
    <div class="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
        <form wire:submit.prevent="authenticate" class="w-full px-2">
            <div>
                <label for="email" class="block text-sm font-medium text-gray-700 leading-5">
                    {{ __('email') }}
                </label>

                <div class="mt-1 rounded-md shadow-sm">
                    <input wire:model.lazy="email" id="email" name="email" type="email" required autofocus
                           class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('email') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror"/>
                </div>

                @error('email')
                <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <div class="mt-6">
                <label for="password" class="block text-sm font-medium text-gray-700 leading-5">
                    {{ __('password') }}
                </label>

                <div class="mt-1 rounded-md shadow-sm">
                    <input wire:model.lazy="password" id="password" type="password" required
                           class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('password') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror"/>
                </div>

                @error('password')
                <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <div class="flex items-center justify-between mt-6">
                <div class="flex items-center">
                    <input wire:model.lazy="remember" id="remember" type="checkbox"
                           class="form-checkbox w-4 h-4 text-indigo-600 transition duration-150 ease-in-out"/>
                    <label for="remember" class="block ml-2 text-sm text-gray-900 leading-5">
                        {{ __('remember') }}
                    </label>
                </div>

                <div class="text-sm leading-5 ml-1">
                    <a href="{{ route('password.request') }}"
                       class="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                        {{ __('forget your password') }}?
                    </a>
                </div>
            </div>

            <div class="mt-6">
                <span class="block w-full rounded-md shadow-sm">
                    <x-btn submit class="w-full">{{ __('sign in') }}</x-btn>
                </span>
            </div>

            @if(\Nwidart\Modules\Facades\Module::has('Social'))
                <hr class="my-4"/>
                <div class="flex justify-center my-2">
                    <div class="space-x-2 flex text-sm font-medium">
                        @foreach(\Modules\Social\Models\UserSocial::getSocialLoginWays() as $way)
                        <a href="{{ route('oauth', $way['name']) }}"
                           class="w-35 p-2 {{ $way['class'] }} text-white rounded-full shadow-md transform hover:scale-120 duration-500 focus:outline-none cursor-pointer">
                            <x-dynamic-component :component="'icon-'.$way['name']" style="width:25px" />
                        </a>
                        @endforeach
                    </div>
                </div>
            @endif

        </form>
    </div>

</div>

