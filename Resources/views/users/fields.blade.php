<x-wd-formItem label="{{ __('main.name') }}">
<x-wd-input name="name" :value="$user->name??''" label="name" required></x-input>
</x-wd-formItem>

<x-wd-formItem label="{{ __('main.department') }}">
<x-wd-input.select name="department" :value="$user->department??''" :optionHtml="\Modules\Core\Models\Department::getOptionsHtml(isset($user->departments[0])?$user->departments[0]->id:0)" label="department" required>
</x-input.select>
</x-wd-formItem>

<x-wd-formItem label="{{ __('main.mobile') }}">
<x-wd-input name="mobile" :value="$user->mobile??''" label="mobile" required></x-input>
</x-wd-formItem>

<x-wd-formItem label="{{ __('main.email') }}">
<x-wd-input type="email" name="email" :value="$user->email??''" label="email" required></x-input>
</x-wd-formItem>

<x-wd-formItem label="{{ __('avatar') }}">
    <input type="file" name="avatar">
</x-wd-formItem>

<x-wd-formItem label="{{ __('main.password') }}">
<x-wd-input type="password" name="password" label="password"></x-input>
</x-wd-formItem>


<div class="layui-form-item layui-layout-admin">
    <div class="layui-input-block">
		<div class="layui-footer z-50" style="left:0;">
			<button class="layui-btn" lay-submit lay-filter="component-form-demo1">{{__('submit')}}</button>
			<button type="reset" class="layui-btn layui-btn-primary" lay-submit-cancel>{{__('cancel')}}</button>
		</div>
    </div>
</div>

@push('script')
	<script>
		layui.use('form', function(){
			var form = layui.form
		})
	</script>
@endpush
