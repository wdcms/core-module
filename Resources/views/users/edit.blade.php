@extends('core::layouts.master')

@section('content')
    <x-wd-card>
        <x-wd-form :action="route('users.update', $user->id)" method="patch">
            @include('core::users.fields')
        </x-form>
    </x-card>
@endsection
