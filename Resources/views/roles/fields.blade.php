<x-wd-formItem label="name">
<x-wd-input name="name" :value="$role->name??''"/>
</x-wd-formItem>

<x-wd-formItem label="label">
<x-wd-input name="label" :value="$role->label??''"/>
</x-wd-formItem>

<x-wd-formItem label="guard_name">
<x-wd-input.select name="guard_name" required :options="['web']" :value="$permission->guard_name??'web'" />
</x-wd-formItem>


<div class="layui-form-item layui-layout-admin">
    <div class="layui-input-block">
		<div class="layui-footer z-50" style="left:0;">
			<button class="layui-btn" lay-submit lay-filter="component-form-demo1">{{__('submit')}}</button>
			<button type="reset" class="layui-btn layui-btn-primary">{{__('cancel')}}</button>
		</div>
    </div>
</div>

@push('script')
	<script>
		layui.use('form', function(){
			var form = layui.form
		})
	</script>
@endpush
