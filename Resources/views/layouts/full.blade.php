@include('core::layouts._head')

<div id="lumina_app">
    @include('core::message.flash')

    @yield('submenu')
    @yield('content')
</div>

@include('core::layouts._foot')
