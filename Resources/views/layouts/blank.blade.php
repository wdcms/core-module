@include('core::layouts._head')

@include('core::message.flash')
@yield('content')

@include('core::layouts._foot')

