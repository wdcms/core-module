@include('core::layouts._head')

<main class="min-h-screen bg-white p-6">
    @include('core::message.flash')
    @yield('content')
</main>

@include('core::layouts._foot')
