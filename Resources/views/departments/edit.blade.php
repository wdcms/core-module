@extends('core::layouts.master')

@section('content')
    <x-wd-card>
        <x-wd-form :action="route('departments.update', $department->id)" method="patch">
            @include('core::departments.fields')
        </x-form>
    </x-card>

@endsection
