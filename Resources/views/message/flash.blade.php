@if (session('status'))

<div id="g-alert">
    <div class="alert-bd">
        <div class="bd shadow border rounded-lg">
{{--            <button type="button"--}}
{{--                    class="fa fa-times"--}}
{{--                    data-dismiss="alert"--}}
{{--                    aria-hidden="true"--}}
{{--            ></button>--}}
            <span class="text-green-400 font-bold">{{ session('status') }}</span>
        </div>
    </div>
</div>

@push('script')
    <script>
        $(function(){
            $(".alert-bd").on("webkitAnimationEnd", function(){
                var $self = $(this)
                setTimeout(function(){
                    $self.animate({top: '-30px'}, 'slow', '', function(){
                        $self.remove()
                    })
                }, 2000)
            })
        })
    </script>
@endpush

@endif
