<?php

namespace Modules\Core\View\Components;

use Illuminate\View\Component;

class GuestLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\Viewold
     */
    public function render()
    {
        return view('layouts.guest');
    }
}
