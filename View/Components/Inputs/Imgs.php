<?php

namespace Modules\Core\View\Components\Inputs;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class Imgs extends Component
{
    public $name;
    public $type;
    public $label;
    public $verify;
    public $value;
    public $limit;

    public $iptkey;


    /**
     * Imgs constructor.
     * @param $name
     * @param string $type
     * @param string $verify
     * @param string $value
     * @param int $limit
     */
    public function __construct(string $name, string $type = 'text', string $verify = '', string $value = '', int $limit = 1)
    {
        $this->name = $name;
        $this->type = $type;
        $this->verify = $verify;

        $this->value = $value ?: old($name);
        $this->limit = $limit;

        $this->iptkey = Str::random(6);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\Viewold|string
     */
    public function render()
    {
        return  <<<'blade'
            <div class="layui-form-img" lay-filter="ximg" data-limit="{{$limit}}">
                <input type="hidden"
                    name="{{$name}}"
                    @if($value)value="{{$value}}"@endif
                    class="layui-input"
                    @if($verify)lay-verify="{{$verify}}"@endif />
            </div>
        blade;
    }

}
