<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Core\Http\Controllers\Api\RolesController;
use Modules\Core\Http\Controllers\Api\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middlewire' => 'auth:sanctum'], function(){

    Route::get('/users', [UsersController::class, 'index']);

    // 用户管理
    Route::post('/users/export', [UsersController::class, 'export'])->name('user.export');
    Route::post('/roles/attach', [RolesController::class, 'attach'])->name('role.attach');
    Route::post('/roles/detach', [RolesController::class, 'detach'])->name('role.detach');

    // 消息管理
//    Route::get('/notification', 'NotificationController@index')->name('notification.index');
//    Route::get('/notification/count', 'NotificationController@count')->name('notification.count');
//    Route::post('/notification/markread', 'NotificationController@markread')->name('notification.markread');
//    Route::post('/notification/delete', 'NotificationController@delete')->name('notification.delete');

    // job
//    Route::get('/job/status', 'JobController@status')->name('job.status');

    // 上传
//    Route::post('upload', [UploadController::class, 'upload'])->name('upload');

    // 文件管理
//    Route::get('file', 'FileController@index')->name('file.index');
//    Route::post('file', 'FileController@store')->name('file.store');
//    Route::post('file/get', 'FileController@get')->name('file.get');
//    Route::post('file/delete', 'FileController@delete')->name('file.delete');
//    Route::post('file/rename', 'FileController@rename')->name('file.rename');
//    Route::post('file/create', 'FileController@create')->name('file.create');

    // 日程管理
//    Route::get('/calendars', 'CalendarController@index')->name('calendar.index');

});
