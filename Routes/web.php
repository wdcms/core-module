<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Core\Http\Controllers\Auth\EmailVerificationController;
use Modules\Core\Http\Controllers\Auth\LogoutController;
use Modules\Core\Http\Controllers\DepartmentsController;
use Modules\Core\Http\Controllers\FileController;
use Modules\Core\Http\Controllers\NotificationController;
use Modules\Core\Http\Controllers\OptionsController;
use Modules\Core\Http\Controllers\PermissionsController;
use Modules\Core\Http\Controllers\RolesController;
use Modules\Core\Http\Controllers\UserAddressesController;
use Modules\Core\Http\Controllers\UsersController;
use Modules\Core\Http\Livewire\Auth\Login;
use Modules\Core\Http\Livewire\Auth\Passwords\Confirm;
use Modules\Core\Http\Livewire\Auth\Passwords\Email;
use Modules\Core\Http\Livewire\Auth\Passwords\Reset;
use Modules\Core\Http\Livewire\Auth\Register;
use Modules\Core\Http\Livewire\Auth\Verify;

Route::middleware('guest')->group(function () {
    Route::get('login', Login::class)->name('login');
    Route::get('register', Register::class)->name('register');
});

Route::get('password/reset', Email::class)->name('password.request');
Route::get('password/reset/{token}', Reset::class)->name('password.reset');

Route::middleware('admin:core')->group(function () {
    Route::get('email/verify', Verify::class)->middleware('throttle:6,1')->name('verification.notice');
    Route::get('password/confirm', Confirm::class)->name('password.confirm');
    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)->middleware('signed')->name('verification.verify');

    Route::post('logout', LogoutController::class)->name('logout');

    Route::view('/', 'core::index')->name('home');
    Route::view('/dashboard', 'core::dashboard')->name('dashboard');

    // file
    Route::get('/md', [FileController::class, 'md'])->name('file.md');

    // optios
    Route::group(['prefix' => 'option', 'as' => 'option.'], function(){

        Route::get('/', [OptionsController::class, 'index'])->name('index');
        Route::post('/', [OptionsController::class, 'store']);
    });

    // 消息管理
    Route::get('/notification', [NotificationController::class, 'index'])->name('notification.index');

    // 权限管理
    Route::post('permission/attach', [PermissionsController::class, 'attach'])->name('permission.attach');
    Route::resource('permission', PermissionsController::class);
    Route::resource('roles', RolesController::class);

    Route::match(['get', 'post'], 'users/profile', [UsersController::class, 'profile'])->name('user.profile');
    Route::match(['get', 'post'], 'users/resetpasswd', [UsersController::class, 'resetpasswd'])->name('user.resetpasswd');

    Route::resource('users', UsersController::class);
    Route::resource('departments', DepartmentsController::class);
    Route::resource('user-addresses', UserAddressesController::class);
});
