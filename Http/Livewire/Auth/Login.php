<?php

namespace Modules\Core\Http\Livewire\Auth;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    /** @var string */
    public $email = '';

    /** @var string */
    public $password = '';

    /** @var bool */
    public $remember = false;

    public $tenant_id = '';

    protected $rules = [
        'email' => ['required', 'email'],
        'password' => ['required'],
    ];

    public function mount()
    {
        $this->tenant_id = tenant('id');
    }

    public function authenticate()
    {
        $this->validate();

        if (!Auth::attempt([
            'email' => $this->email,
            'password' => $this->password,
            'tenant_id' => $this->tenant_id
        ], $this->remember)) {
            $this->addError('email', trans('auth.failed'));

            return false;
        }

        return redirect()->intended(route('home'));
    }

    public function render()
    {
        return view('core::livewire.auth.login')->extends('layouts.full');
    }
}
