<?php

namespace Modules\Core\Http\Livewire\Auth;

use Modules\Core\Providers\RouteServiceProvider;
use Modules\Core\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Livewire\Component;

class Register extends Component
{
    /** @var string */
    public $name = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $password = '';

    public $tenant_id = '';

    /** @var string */
    public $passwordConfirmation = '';

    public function mount()
    {
        $this->tenant_id = tenant('id');
    }

    public function register()
    {
        $this->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:core_users'],
            'password' => ['required', 'min:6', 'same:passwordConfirmation'],
        ]);

        $user = User::create([
            'email' => $this->email,
            'name' => $this->name,
            'tenant_id' => $this->tenant_id,
            'password' => Hash::make($this->password),
        ]);

        event(new Registered($user));

        Auth::login($user, true);

        return redirect()->intended(route('home'));
    }

    public function render()
    {
        return view('core::livewire.auth.register')->extends('layouts.auth');
    }
}
