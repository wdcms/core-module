<?php

namespace Modules\Core\Http\Resources;

class UserResource extends BaseResource
{
    public function toArray($request)
    {
        if($request->get('dataType') == 'xselect') {
            $_value = $request->get('value');
            return [
                'name' => !empty($this->name)?$this->name:(!empty($this->username)?$this->username:$this->nickname),
                'value' => $this->id,
                'selected' => !$_value ? false : in_array($this->id, explode(',', $_value))
            ];
        }else{
            return [
                'id' => $this->id,
                'name' => !empty($this->name)?$this->name:(!empty($this->username)?$this->username:$this->nickname),
                'avatar' => $this->avatar,
                'email' => $this->email,
                'mobile' => $this->mobile,
                'is_admin' => $this->is_admin,
                'last_login_at' => $this->last_login_at,
                'roles' => RoleResource::collection($this->roles)
            ];
        }
    }
}
