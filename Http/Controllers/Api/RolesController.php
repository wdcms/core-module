<?php
/**
 * Created by PhpStorm.
 * User: jory
 * Date: 2019/4/3
 * Time: 18:21
 */

namespace Modules\Core\Http\Controllers\Api;


use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Models\Role;
use Modules\Core\Models\User;

class RolesController extends BaseController
{

    protected $model;

    /**
     * UsersController constructor.
     * @param UserRepository $model
     */
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attach(Request $request)
    {
        $request->validate(['role_id'=>'required', 'ids' =>'required']);

        try{
            $role = $this->model->find($request->get('role_id'));
            foreach (explode(',', $request->get('ids')) as $_id) {
                $_user = User::find($_id);
                $_user->assignRole($role);
            }
            return $this->toResponse([], 'success');
        }catch (\Exception $e){
            return $this->toException($e);
        }
    }

    public function detach(Request $request)
    {
        $request->validate(['role_id'=>'required', 'ids' =>'required']);

        try{
            $role = $this->model->find($request->get('role_id'));
            foreach (explode(',', $request->get('ids')) as $_id) {
                $_user = User::find($_id);
                $_user->removeRole($role);
            }
            return $this->toResponse([], 'success');
        }catch (\Exception $e){
            return $this->toException($e);
        }
    }
}
