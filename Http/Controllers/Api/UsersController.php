<?php
/**
 * Created by PhpStorm.
 * User: jory
 * Date: 2019/4/3
 * Time: 18:21
 */

namespace Modules\Core\Http\Controllers\Api;


use Illuminate\Http\Request;
use Modules\Core\Exports\UsersExport;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Http\Resources\UserResource;
use Modules\Core\Models\User;

class UsersController extends BaseController
{

    protected $model;

    /**
     * UsersController constructor.
     * @param UserRepository $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $limit = $request->get('search') ? 100 : \request('limit', 10);
        $users = $this->model->filter($request)->paginate($limit);

        return $this->toCollection($users, UserResource::class);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function export(Request $request)
    {
        $request->validate(['ids' =>'required']);
        $field = [
            'id' => 'USERID',
            'name' => '姓名',
            'mobile' => '电话',
            'created_at' =>'入职时间'
        ];
        return $this->toAjaxExport(new UsersExport($request, ['field' => $field]));
    }
}
