<?php


namespace Modules\Core\Http\Controllers;


use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class FileController extends BaseController
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function md(Request $request)
    {
        $request->validate(['path' => 'required']);
        $_doc = (new Filesystem())->get(urldecode($request->get('path')));

        $html = markdown_converter($_doc);
        return view('file.doc', compact('html'));
    }

}
