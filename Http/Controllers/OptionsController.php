<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Stancl\Tenancy\Tenancy;

class OptionsController extends BaseController
{

    public function index()
    {
        return view("core::options.index");
    }

    public function store(Request $request)
    {
        foreach ($request->get('option') as $_key => $_option) {
            if($_option !== null) {
                option([$_key => $_option]);
            }
        }
        return redirect()->back()->with('status', '配置保存成功，若未生效请手动清除缓存!');
    }

}
