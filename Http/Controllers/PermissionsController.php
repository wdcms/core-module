<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Modules\Core\Models\Role;
use Modules\Core\Models\Permission;
use Modules\Core\Http\Requests\PermissionRequest;

/**
 * Class PermissionsController.
 *
 * @package namespace Modules\Core\Http\Controllers;
 */
class PermissionsController extends BaseController
{
    /**
     * @var Permission
     */
    protected $model;

    public function __construct(Permission $model)
    {
        $this->model = $model;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $role_id = $request->get('role_id');
        $role = $role_id?Role::findOrFail($role_id):Role::first();
        $role_guard = isset($role)?$role->guard_name:'web';
        return view('core::permissions.index', compact('role', 'role_guard'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        // $this->authorize('create', Permission::class);
        return view('core::permissions.create');
    }


    /**
     * @param PermissionRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(PermissionRequest $request)
    {
        try {
            $this->model->create($request->all());

            return redirect()->back()->with('status', 'create success');
        } catch (ValidationException $e) {
            return $this->toException($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = $this->model->find($id);
        // $this->authorize('view', $permission);
        return $this->toTable($permission);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = $this->model->find($id);
        // $this->authorize('update', $permission);

        return view('core::permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *

     */
    public function update(PermissionRequest $request, $id)
    {
        try {
            $model = $this->model->find($id);
            // $this->authorize('update', $model);

            $model->fill($request->all())->save();
            return redirect()->back()->with('status', 'update success');
        } catch (ValidationException $e) {
            return $this->toException($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->model->find($id);
        // $this->authorize('delete', $model);
        $model->delete();

        return $this->toResponse([], '删除成功');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attach(Request $request)
    {
        $request->validate(['role_id' => 'required']);

        $role = Role::find($request->get('role_id'));

        $role->permissions()->detach();
        $role->permissions()->attach($request->get('permission'));

        return redirect()->back()->with('status', 'save success');
    }
}
