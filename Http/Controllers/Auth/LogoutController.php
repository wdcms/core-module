<?php

namespace Modules\Core\Http\Controllers\Auth;

use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class LogoutController extends BaseController
{
    public function __invoke(): RedirectResponse
    {
        Auth::logout();

        return redirect(route('home'));
    }
}
