<?php

namespace Modules\Core\Actions;

use App\Traits\AsAction;
use Illuminate\Support\Facades\Cache;
use QL\QueryList;
use Illuminate\Support\Arr;


class EveryDayInspired
{
    use AsAction;

    /**
     * 获取每日一文
     *
     * @return void
     */
    public function handle()
    {
        $cache_key = 'inspired_'.now()->day;
        $html = Cache::tags('global')->get($cache_key) ?? [];

        if(!$html) {
            $newList = QueryList::get('http://wufazhuce.com')->rules([
                'content' => ['.fp-one-cita-wrapper>.fp-one-cita', 'text'],
                'img'  => ['img.fp-one-imagen', 'src']
            ])->range('#carousel-one>.carousel-inner>.item.active')->queryData();
            $res = $newList[0] ?? [
                    'content' => '有勇气用理性去处理一切',
                    'img' => 'http://cdn.xbhub.com/img/svg/'.Arr::random(['1','2','3','4']).'.svg'
                ];

            $html = sprintf(
                '<div class="w-full h-full rounded-l-lg bg-cover relative"><img src="%s" class="block w-full h-full"/>'.
                '<div class="w-full bg-gray-900 bg-opacity-75 absolute bottom-0 p-4 text-white text-sm">%s</div></div>', $res['img'], $res['content']);

            Cache::tags('global')->put($cache_key, $html, now()->addHours(6));
        }
        return $html;
    }
}
