<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_users', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('nickname')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mobile')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->string('avatar', 500)->nullable();

            $table->ipAddress('create_ip_at')->nullable()->comment('创建ip');
            $table->timestamp('last_login_at')->nullable()->comment('最后一次登录时间');
            $table->ipAddress('last_login_ip_at')->nullable()->comment('最后一次登录ip');
            $table->integer('login_times')->default(0)->comment('登录次数');

            $table->rememberToken();
            $table->timestamps();
            $table->tenant();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_users');
    }
}
