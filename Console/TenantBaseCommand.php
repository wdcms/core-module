<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Stancl\Tenancy\Tenancy;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TenantBaseCommand extends Command
{

    /**
     * TenantCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->addOption('tenant_id', null, InputOption::VALUE_REQUIRED, "Tenant ID");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        if(!$this->option('tenant_id')) {
            throw new InvalidOptionException("The \"tenant_id\" option does not exist.");
        }

        Tenancy()->initialize($this->option('tenant_id'));
    }

}
