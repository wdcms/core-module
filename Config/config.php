<?php

return [
    'name' => 'Core',
    // https://cdn.jsdelivr.net/npm/lumina.js@3.1.0/dist
    'lumina_js_cdn' => 'https://cdn.jsdelivr.net/npm/lumina.js@3.2.0/dist',
//    'lumina_js_cdn' => app()->isLocal()?asset('lumina'):'https://cdn.jsdelivr.net/npm/lumina.js@3.1.0/dist',
    'origanization_permission' => 1, //开启组织系统(0关闭，1开启)
];
